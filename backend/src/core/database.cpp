
#include <map>
#include <iostream>

#include "database.hpp"

#include "globals.hpp"
#include "exception.hpp"

using namespace backend::core;

Database::Database() {
  // empty
}

/* Easy way to get vaues from request json.
 * { "data": { "<field>": ? } }
 */
const std::string Database::read(const std::shared_ptr<const nlohmann::json> request, const std::string &field) const {
  const nlohmann::json &data = (*request)["data"];

  if (data.find(field) == data.end()) {
    throw backend::core::Exception("Missing argument while parsing request data");
  }
  return data[field];
}

/* Each API request have token of the calling user.
 * This method is checking if this user is valid.
 * Method converts token to user_id.
 */
const backend::core::object::User::Ptr Database::auth(const std::shared_ptr<const nlohmann::json> request) {
  if (request->find("token") == request->end()) {
    throw backend::core::Exception("Token missing in request data");
  }

  const backend::common::Hash token = (*request)["token"];

  const backend::core::object::User::Ptr user = _userManager.auth(token);

  return user;
}

const nlohmann::json Database::selectUserCreate(const std::shared_ptr<const nlohmann::json> request) {
  try {
    std::lock_guard<std::mutex> lock(_mutex);

    const std::string username = read(request, "name");

    backend::core::object::User::Ptr user = std::make_shared<backend::core::object::User>();
    user->name(username);

    // TODO: what if user with duplicate name created?

    _userManager.add(user);

    return nlohmann::json({
      {"status", "ok"},
      {"token", user->token().str()}
    });
  }
  catch (backend::core::Exception e) {
    Globals::logger.push(common::Logger::Status::Warning, std::string(e.what()));
    return nlohmann::json({
      {"status", "error"},
      {"message", std::string(e.what())}
    });
  }
}

const nlohmann::json Database::selectGroupCreate(const std::shared_ptr<const nlohmann::json> request) {
  try {
    std::lock_guard<std::mutex> lock(_mutex);

    const backend::core::object::User::Ptr user = auth(request);
    const std::string groupname = read(request,"name");

    backend::core::object::Group::Ptr group = std::make_shared<backend::core::object::Group>(user);
    group->name(groupname);

    _groupManager.add(group);

    return nlohmann::json({
      {"status", "ok"},
      {"group_id", group->id().str()}
    });
  }
  catch (backend::core::Exception e) {
    Globals::logger.push(common::Logger::Status::Warning, std::string(e.what()));
    return nlohmann::json({
      {"status", "error"},
      {"message", std::string(e.what())}
    });
  }
}

const nlohmann::json Database::selectGroupGrant(const std::shared_ptr<const nlohmann::json> request) {
  try {
    std::lock_guard<std::mutex> lock(_mutex);

    const backend::core::object::User::Ptr user = auth(request);
    const std::string groupId = read(request,"group_id");

    const backend::core::object::Group::Ptr group = _groupManager.get(groupId);

    //

    return nlohmann::json({
      {"status", "ok"}
    });
  }
  catch (backend::core::Exception e) {
    Globals::logger.push(common::Logger::Status::Warning, std::string(e.what()));
    return nlohmann::json({
      {"status", "error"},
      {"message", std::string(e.what())}
    });
  }
}

const nlohmann::json Database::selectMessageSend(const std::shared_ptr<const nlohmann::json> request) {
  try {
    std::lock_guard<std::mutex> lock(_mutex);

    const backend::core::object::User::Ptr user = auth(request);
    const std::string groupId = read(request,"group_id");
    const std::string text = read(request,"text");

    const backend::core::object::Group::Ptr group = _groupManager.get(groupId);

    // TODO: check if user have permissions to send

    backend::core::object::Message::Ptr message = std::make_shared<backend::core::object::Message>(user);
    message->text(text);

    group->messages().add(message);

    return nlohmann::json({
      {"status", "ok"},
      {"message_id", message->id().str()}
    });
  }
  catch (backend::core::Exception e) {
    Globals::logger.push(common::Logger::Status::Warning, std::string(e.what()));
    return nlohmann::json({
      {"status", "error"},
      {"message", std::string(e.what())}
    });
  }
}

const nlohmann::json Database::json() {
  std::lock_guard<std::mutex> lock(_mutex);

  const nlohmann::json result({
    {"users", _userManager.json()},
    {"groups", _groupManager.json()}
  });

  return result;
}
