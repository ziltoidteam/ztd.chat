#include <algorithm>
#include <iostream>
#include <vector>

#include "core/exception.hpp"
#include "core/manager/message_manager.hpp"

using namespace backend::core::manager;

Message::Message() {
  // empty
}

void Message::add(const backend::core::object::Message::Ptr message) {
  _messages.push_back(message);
}

const nlohmann::json Message::json() const {
  nlohmann::json messages = nlohmann::json::array();
  for(const auto &message : _messages) {
    messages.push_back(message->json());
  }

  return nlohmann::json(
    messages
  );
}
