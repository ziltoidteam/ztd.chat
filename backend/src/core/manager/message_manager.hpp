#pragma once

#include <list>

#include <nlohmann/json.hpp>

#include "common/hash.hpp"
#include "core/object/message.hpp"

namespace backend {
  namespace core {
    namespace manager {
      class Message {
      public:
        Message();

        void add(const backend::core::object::Message::Ptr message);

        const nlohmann::json json() const;

      private:
        std::list<backend::core::object::Message::Ptr > _messages;
      };
    }
  }
}
