#pragma once

#include <map>

#include <nlohmann/json.hpp>

#include "common/hash.hpp"
#include "core/object/group.hpp"

namespace backend {
  namespace core {
    namespace manager {
      class Group {
      public:
        Group();

        bool exist(const backend::common::Hash &id) const;
        void add(const backend::core::object::Group::Ptr group);
        const backend::core::object::Group::Ptr get(const backend::common::Hash &id);

        const nlohmann::json json() const;

      private:
        std::map<const backend::common::Hash, backend::core::object::Group::Ptr > _groups;
      };
    }
  }
}
