#include <algorithm>
#include <iostream>
#include <vector>

#include "core/exception.hpp"
#include "core/manager/group_manager.hpp"

using namespace backend::core::manager;

Group::Group() {
  // empty
}

/* Function check if user with given key exist in manager
 * Works very fast, can be used for quick check if
 * correct group was sent thruout the API call
 */
bool Group::exist(const backend::common::Hash &id) const {
  return (_groups.count(id) != 0);
}

void Group::add(const backend::core::object::Group::Ptr group) {
  if(exist(group->id()))
    throw Exception("Group with this id already exist");

  _groups.insert(std::pair <const backend::common::Hash, backend::core::object::Group::Ptr>(group->id(), group));
}

const backend::core::object::Group::Ptr Group::get(const backend::common::Hash &id) {
  if(!exist(id))
    throw Exception("Group with this id not exist");

  return _groups.at(id);
}

const nlohmann::json Group::json() const {
  nlohmann::json groups = nlohmann::json::array();
  for(const auto &group : _groups) {
    groups.push_back(group.second->json());
  }

  return nlohmann::json(
    groups
  );
}
