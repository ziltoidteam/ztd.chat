#pragma once

#include <map>

#include <nlohmann/json.hpp>

#include "common/hash.hpp"
#include "core/object/user.hpp"

namespace backend {
  namespace core {
      namespace manager {
        class User {
        public:
          User();

          bool exist(const backend::common::Hash &id) const;
          void add(const backend::core::object::User::Ptr user);

          const backend::core::object::User::Ptr auth(const backend::common::Hash &token );

          const nlohmann::json json() const;
        private:
          std::map<const backend::common::Hash, backend::core::object::User::Ptr > _users;
        };
      }
  }
}
