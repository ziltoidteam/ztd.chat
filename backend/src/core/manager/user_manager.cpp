#include <algorithm>
#include <iostream>
#include <vector>

#include "core/exception.hpp"
#include "core/manager/user_manager.hpp"

using namespace backend::core::manager;

User::User() {
  // empty
}

/* Function check if user with given key exist in manager
 * Works very fast, can be used for quick check if
 * correct user was sent thruout the API call
 */
bool User::exist(const backend::common::Hash &id) const {
  return (_users.count(id) != 0);
}

void User::add(const backend::core::object::User::Ptr user) {
  if(exist(user->id()))
    throw Exception("Username with this id already exist");

  _users.insert(std::pair <const backend::common::Hash, backend::core::object::User::Ptr>(user->id(), user));
}

/* Function return an user with given token
 * And throw an exception if user not exist.
 */
const backend::core::object::User::Ptr User::auth(const backend::common::Hash &token ) {
  for(const auto it : _users) {
    const backend::core::object::User::Ptr &user = it.second;
    if (user->token() == token)
      return user;
  }

  throw Exception("Token is not associated with any user");
}

const nlohmann::json User::json() const {
  nlohmann::json users = nlohmann::json::array();
  for(const auto &user : _users) {
    users.push_back(user.second->json());
  }

  return nlohmann::json(
    users
  );
}
