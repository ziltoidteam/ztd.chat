#pragma once

namespace backend {
  namespace core
  {
    class Exception : public std::exception {
        public:
          Exception(const char *msg) : err_msg(msg) {};
          ~Exception() throw() {};
          const char *what() const throw() { return this->err_msg.c_str(); };
        private:
          std::string err_msg;
    };
  }
}
