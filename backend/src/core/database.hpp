#pragma once

#include <mutex>
#include <string>
#include <nlohmann/json.hpp>

#include "core/manager/user_manager.hpp"
#include "core/manager/group_manager.hpp"

namespace backend
{
  namespace core
  {
    class Database {
    public:
      Database();

      const nlohmann::json selectUserCreate(const std::shared_ptr<const nlohmann::json> request);

      const nlohmann::json selectGroupCreate(const std::shared_ptr<const nlohmann::json> request);
      const nlohmann::json selectGroupGrant(const std::shared_ptr<const nlohmann::json> request);

      const nlohmann::json selectMessageSend(const std::shared_ptr<const nlohmann::json> request);

      const nlohmann::json json();
    private:
      const std::string read(const std::shared_ptr<const nlohmann::json> request, const std::string &field) const;
      const backend::core::object::User::Ptr auth(const std::shared_ptr<const nlohmann::json> request);

      backend::core::manager::User _userManager;
      backend::core::manager::Group _groupManager;
      std::mutex _mutex;
    };
  }
}
