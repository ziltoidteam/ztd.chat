#pragma once

#include <string>
#include <memory>

#include "core/object/object.hpp"
#include "core/object/user.hpp"

#include "core/manager/user_manager.hpp"
#include "core/manager/message_manager.hpp"

namespace backend {
  namespace core {
    namespace object {
      class Group : public Object {
        public:
            Group(const backend::core::object::User::Ptr owner);

            const std::string name() const;
            void name(const std::string &name);

            const backend::core::object::User::Ptr owner() const;

            backend::core::manager::Message &messages();

            const nlohmann::json json() const;

            typedef std::shared_ptr<Group> Ptr;
        private:
            const backend::core::object::User::Ptr _owner;
            std::string _name;
            backend::core::manager::Message _messageManager;
            backend::core::manager::User _grantsManager;
      };
    }
  }
}
