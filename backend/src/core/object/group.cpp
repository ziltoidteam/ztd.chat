#include "core/object/group.hpp"

using namespace backend::core::object;

Group::Group(const backend::core::object::User::Ptr owner)
    : _owner(owner) {
      // empty
}

const std::string Group::name() const {
    return _name;
}

void Group::name(const std::string &name) {
    _name = name;
}

const backend::core::object::User::Ptr Group::owner() const {
  return _owner;
}

backend::core::manager::Message &Group::messages(){
  return _messageManager;
}

const nlohmann::json  Group::json() const {
  return nlohmann::json({
    {"id",this->id().str()},
    {"owner", this->owner()->id().str()},
    {"name", this->name()},
    {"messages", this->_messageManager.json()},
    {"created", created().str()}
  });
}
