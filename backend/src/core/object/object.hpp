
#pragma once

#include <nlohmann/json.hpp>

#include "common/time.hpp"
#include "common/hash.hpp"

namespace backend {
  namespace core {
    namespace object {
      class Object {
        public:
            Object();

            const backend::common::Hash &id() const;
            const backend::common::Time &created() const;

            virtual const nlohmann::json json() const = 0;
        private:
            const backend::common::Hash _id;
            const backend::common::Time _created;
      };
    }
  }
}
