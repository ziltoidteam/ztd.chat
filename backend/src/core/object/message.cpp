#include "core/object/message.hpp"

using namespace backend::core::object;

Message::Message(const backend::core::object::User::Ptr owner)
    : _owner(owner) {
      // empty
}

const std::string &Message::text() const {
    return _text;
}

void Message::text(const std::string &text) {
    _text = text;
}

const backend::core::object::User::Ptr Message::owner() const {
  return _owner;
}

const nlohmann::json  Message::json() const {
  return nlohmann::json({
    {"id",this->id().str()},
    {"owner",this->owner()->id().str()},
    {"text", this->text()},
    {"created", created().str()}
  });
}
