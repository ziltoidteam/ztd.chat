
#pragma once

#include <string>
#include <memory>

#include "core/object/object.hpp"
#include "core/object/user.hpp"

namespace backend {
  namespace core {
    namespace object {
      class Message : public Object {
        public:
            Message(const backend::core::object::User::Ptr owner);

            const std::string &text() const;
            void text(const std::string &text);

            const backend::core::object::User::Ptr owner() const;

            const nlohmann::json json() const;

            typedef std::shared_ptr<Message> Ptr;
        private:
            const backend::core::object::User::Ptr _owner;
            std::string _text;
      };
    }
  }
}
