
#pragma once

#include <string>
#include <memory>

#include "core/object/object.hpp"

namespace backend {
  namespace core {
    namespace object {
      class User : public Object {
        public:
            User();

            const std::string name() const;
            void name(const std::string &name);

            const backend::common::Hash &token() const;
            void token(const backend::common::Hash &token);

            const nlohmann::json json() const;

            typedef std::shared_ptr<User> Ptr;
        private:
            backend::common::Hash _token;
            std::string _name;
      };
    }
  }
}
