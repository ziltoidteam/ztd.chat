#include "core/object/object.hpp"

using namespace backend::core::object;

Object::Object()
    : _id(backend::common::Hash(24)),
      _created() {
      // empty
}

const backend::common::Hash &Object::id() const {
  return _id;
}

const backend::common::Time &Object::created() const {
  return _created;
}
