#include "core/object/user.hpp"

using namespace backend::core::object;

User::User()
    : _token(backend::common::Hash(24)) {
      // empty
}

const std::string User::name() const {
    return _name;
}

void User::name(const std::string &name) {
    _name = name;
}

const backend::common::Hash &User::token() const {
  return _token;
}

void User::token(const backend::common::Hash &token) {
  _token = token;
}

const nlohmann::json  User::json() const {
  return nlohmann::json({
    {"id",this->id().str()},
    {"token",this->token().str()},
    {"name", this->name()},
    {"created", created().str()}
  });
}
