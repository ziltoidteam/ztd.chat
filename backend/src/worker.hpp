
#pragma once

#include <string>
#include <memory>

#include <nlohmann/json.hpp>

namespace backend
{
    class Worker
    {
     public:
        Worker(const std::string &object, const std::string &action, const std::string &request);
        ~Worker();

        const nlohmann::json operator()() const;
     private:
        const nlohmann::json selectUser(const std::shared_ptr<const nlohmann::json> request) const;
        const nlohmann::json selectUserCreate(const std::shared_ptr<const nlohmann::json> request) const;

        const nlohmann::json selectGroup(const std::shared_ptr<const nlohmann::json> request) const;
        const nlohmann::json selectGroupCreate(const std::shared_ptr<const nlohmann::json> request) const;
        const nlohmann::json selectGroupGrant(const std::shared_ptr<const nlohmann::json> request) const;

        const nlohmann::json selectMessage(const std::shared_ptr<const nlohmann::json> request) const;
        const nlohmann::json selectMessageSend(const std::shared_ptr<const nlohmann::json> request) const;

        const std::string &_object;
        const std::string &_action;
        const std::string &_request;
    };


}
