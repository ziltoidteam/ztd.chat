#include <iostream>
#include <thread>
#include <sstream>

#include <corvusoft/restbed/status_code.hpp>
#include <corvusoft/restbed/request.hpp>
#include <corvusoft/restbed/session.hpp>

#include "globals.hpp"

#include "worker.hpp"
#include "core/database.hpp"

using namespace backend;

Worker::Worker(const std::string &object, const std::string &action, const std::string &request)
	: _object(object), _action(action), _request(request) {
  // empty
}

Worker::~Worker() {
  // empty
}

const nlohmann::json Worker::operator()() const {

  std::cout << "Worker is on " << _request << std::endl;
	std::cout << "Worker started. object: " << _object << ", action: " << _action << ", request: " << _request << std::endl;

  std::shared_ptr<const nlohmann::json> request;
	try {
  	request = std::make_shared<const nlohmann::json>(nlohmann::json::parse(_request));
  }
	catch(std::invalid_argument e) {
		Globals::logger.push(common::Logger::Status::Warning, "Fail parsing request json");
		return nlohmann::json({ {"status", "error"}, {"message", "Fail parsing request json"}});
	}

	// Each request should have data field.
	if(request->find("data") == request->end() || !(*request)["data"].is_object()){
		Globals::logger.push(common::Logger::Status::Warning, "Missing data section in request");
		return nlohmann::json({ {"status", "error"}, {"message", "Missing data section in request"}});
	}

	if(_object == "user")
		return selectUser(request);
	else if(_object == "group")
	  return selectGroup(request);
	else if(_object == "message")
	  return selectMessage(request);

	Globals::logger.push(common::Logger::Status::Warning, "Invalid object resource");
	return nlohmann::json({ {"status", "error"}, {"message", "Invalid object resource"}});
}

const nlohmann::json Worker::selectUser(const std::shared_ptr<const nlohmann::json> request) const {
	if(_action == "create")
		return selectUserCreate(request);

	Globals::logger.push(common::Logger::Status::Warning, "Invalid object action");
  return nlohmann::json({ {"status", "error"}, {"message", "Invalid object action"}});
}

const nlohmann::json Worker::selectUserCreate(const std::shared_ptr<const nlohmann::json> request) const {
	return Globals::database.selectUserCreate(request);
}

const nlohmann::json Worker::selectGroup(const std::shared_ptr<const nlohmann::json> request) const {
	if(_action == "create")
		return selectGroupCreate(request);
	else if(_action == "grant")
		return selectGroupGrant(request);

	Globals::logger.push(common::Logger::Status::Warning, "Invalid object action");
  return nlohmann::json({ {"status", "error"}, {"message", "Invalid object action"}});
}

const nlohmann::json Worker::selectGroupCreate(const std::shared_ptr<const nlohmann::json> request) const {
	return Globals::database.selectGroupCreate(request);
}

const nlohmann::json Worker::selectGroupGrant(const std::shared_ptr<const nlohmann::json> request) const {
	return Globals::database.selectGroupGrant(request);
}

const nlohmann::json Worker::selectMessage(const std::shared_ptr<const nlohmann::json> request) const {
	if(_action == "send")
		return selectMessageSend(request);

	Globals::logger.push(common::Logger::Status::Warning, "Invalid object action");
  return nlohmann::json({ {"status", "error"}, {"message", "Invalid object action"}});
}

const nlohmann::json Worker::selectMessageSend(const std::shared_ptr<const nlohmann::json> request) const {
	return Globals::database.selectMessageSend(request);
}
