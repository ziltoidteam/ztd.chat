
#include <iostream>

#include "server.hpp"

int main(int argc, char *argv[]) {
  std::cout << "* Ztd.Chat backend started" << std::endl;

  backend::Server server;

  return 0;
}
