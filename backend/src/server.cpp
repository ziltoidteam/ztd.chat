
#include <iostream>
#include <string>
#include <memory>
#include <thread>

#include <corvusoft/restbed/status_code.hpp>
#include <corvusoft/restbed/session.hpp>
#include <corvusoft/restbed/request.hpp>
#include <corvusoft/restbed/resource.hpp>
#include <corvusoft/restbed/service.hpp>
#include <corvusoft/restbed/settings.hpp>
#include <corvusoft/restbed/string.hpp>

#include "globals.hpp"

#include "server.hpp"
#include "worker.hpp"

using namespace backend;

void api_handler( const std::shared_ptr< restbed::Session > session ) {
    const auto request = session->get_request( );

    size_t content_length = 0;
    request->get_header( "Content-Length", content_length );

    session->fetch( content_length, [ request ]( const std::shared_ptr< restbed::Session > session, const restbed::Bytes & body )
    {
        const std::string object = request->get_path_parameter( "object" );
        const std::string action = request->get_path_parameter( "action" );
        const std::string request = restbed::String::to_string(body);

        backend::Worker worker(object, action, request);
        const std::string result = worker().dump();

        const std::multimap<std::string, std::string> headers = {
          { "Content-Length", std::to_string(result.size()) },
          { "Connection", "keep-alive" },
          { "Content-type","application/json" }
        };
        session->close( restbed::OK, result, headers );
    } );
}

void debug_handler( const std::shared_ptr< restbed::Session > session ) {
  const auto request = session->get_request( );

  size_t content_length = 0;
  request->get_header( "Content-Length", content_length );

  session->fetch( content_length, [ request ]( const std::shared_ptr< restbed::Session > session, const restbed::Bytes & body )
  {
      const std::string result = Globals::database.json().dump();

      const std::multimap<std::string, std::string> headers = {
        { "Content-Length", std::to_string(result.size()) },
        { "Connection", "keep-alive" },
        { "Content-type","application/json" }
      };
      session->close( restbed::OK, result, headers );
  } );
}

Server::Server()
{
   auto resourceApi = std::make_shared< restbed::Resource >( );
   // API handling
   resourceApi->set_path( "/api/{object: .*}/{action: .*}" );
   resourceApi->set_method_handler( "POST", api_handler);

   // Debug handling (disable on production)
   auto resourceDebug = std::make_shared< restbed::Resource >( );
   resourceDebug->set_path( "/api/debug" );
   resourceDebug->set_method_handler( "GET", debug_handler);

   auto settings = std::make_shared< restbed::Settings >( );
   settings->set_bind_address("0.0.0.0");
   settings->set_port( 8080 );
   //settings->set_default_header( "Connection", "close" );
   settings->set_worker_limit(10);
   settings->set_connection_limit(10);

   restbed::Service service;
   service.publish( resourceApi );
   service.publish( resourceDebug );
   service.start( settings );
}

Server::~Server()
{
    // empty
}
