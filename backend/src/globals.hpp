#pragma once

#include "common/logger.hpp"
#include "core/database.hpp"

namespace backend
{
    class Globals
    {
        public:
            static common::Logger logger;

            static core::Database database;
        private:
            Globals();
    };
}
