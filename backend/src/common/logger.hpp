#pragma once

#include <string>
#include <mutex>

namespace backend
{
  namespace common
  {
    class Logger {
    public:
      enum class Status {Warning, Error, Critical};

      Logger();

      void push(const Logger::Status &status, const std::string &message);
    private:
      std::mutex _mutex;

      static const std::string statusToString(const Logger::Status &status);
    };
  }
}
