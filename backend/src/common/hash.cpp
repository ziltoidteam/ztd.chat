#include <cstdlib>
#include <algorithm>

#include <cstdlib>
#include <time.h>

#include "hash.hpp"

using namespace backend::common;

Hash::Hash(const unsigned int length)
  : _hash(randomize(length)) {
  // empty
}

Hash::Hash(const Hash &second)
  : _hash(second._hash) {
  // empty
}

Hash::Hash(const std::string &second)
  : _hash(second) {
  // empty
}

const std::string Hash::randomize(const unsigned int length) const {
  srand(time(NULL));
  auto randchar = []() -> char
  {
      const char charset[] =
      "0123456789"
      "abcdefgh";
      const size_t max_index = (sizeof(charset) - 1);
      return charset[ rand() % max_index ];
  };
  std::string str(length,0);
  std::generate( str.begin(), str.end(), randchar );

  return str;
}

const std::string &Hash::str() const {
  return _hash;
}

bool Hash::operator == (const Hash &second) const {
  return this->_hash == second._hash;
}

bool Hash::operator < (const Hash &second) const {
  return this->_hash < second._hash;
}

Hash &Hash::operator = (const Hash &second) {
  if (this == &second) {
      return *this;
  }

  this->_hash = second._hash;

  return *this;
}
