#pragma once

#include <string>

namespace backend
{
  namespace common
  {
    class Hash {
    public:
      Hash(const unsigned int length);
      Hash(const Hash &second);
      Hash(const std::string &second);

      bool operator == (const Hash &second) const;
      bool operator < (const Hash &second) const;

      Hash &operator = (const Hash &second);

      const std::string &str() const;
    private:
      const std::string randomize(const unsigned int length) const;

      std::string _hash;
    };
  }
}
