#pragma once

#include <chrono>
#include <string>

namespace backend
{
  namespace common
  {
    class Time {
    public:
      Time();

      const std::string str() const;
    private:
      const std::chrono::system_clock::time_point _time;
    };
  }
}
