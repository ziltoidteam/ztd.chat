#include <iomanip>      // std::put_time
//#include <ctime>        // std::time_t, struct std::tm, std::localtime
//#include <chrono>       // std::chrono::system_clock
#include <sstream>

#include "time.hpp"

using namespace backend::common;

Time::Time()
  : _time(std::chrono::system_clock::now()){
  // empty
}

const std::string Time::str() const {
  std::stringstream ss;
  const auto created = std::chrono::system_clock::to_time_t(_time);
  ss << std::put_time(std::localtime(&created), "%a %b %d %H:%M:%S %Y");

  return ss.str();
}
