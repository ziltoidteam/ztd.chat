
#include <iostream>
#include <map>

#include "logger.hpp"

using namespace backend::common;

Logger::Logger() {
  // empty
}

const std::string Logger::statusToString(const Logger::Status &status){
  std::map<const Logger::Status, const std::string> transform = {
    { Logger::Status::Warning, "Warning" },
    { Logger::Status::Error, "Error" },
    { Logger::Status::Critical, "Critical" }
  };

  return transform.at(status);
}

void Logger::push(const Logger::Status &status, const std::string &message) {
  std::lock_guard<std::mutex> lock(_mutex);
  std::cout << statusToString(status) << ": " << message << std::endl;
}
